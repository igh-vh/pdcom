/*****************************************************************************
 *
 * $Id$
 *
 * Copyright (C) 2020  Richard Hacker (lerichi at gmx dot net)
 *
 * This file is part of the PdCom library.
 *
 * The PdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The PdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#define PY_SSIZE_T_CLEAN
#include <Python.h>
#include <structmember.h>   // struct PyMemberDef

#include "../pdcom4/Variable.h"
#include "type_traits.h"

#define OBJ(self) ((PyObject*)self)
#define SELF(o)   ((SubscriptionObject*)o)
#define PDCOM(o) (((SubscriptionObject*)o)->pdcom)

void Process_unsubscribe(PdCom::Process*, const PdCom::Variable::Subscription*);
PyObject* Process_getVariableObject(
        PdCom::Process*, const PdCom::Variable*);

////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
typedef struct {
    PyObject_HEAD //;
    const PdCom::Variable::Subscription* pdcom;
    PyObject *process;
    PyObject *subscriber;
    PyObject *(*getValue)(const PdCom::Variable::Subscription*,
            size_t index, size_t nelem);
    int id;
    PyObject *variable;
    int decimation;
} SubscriptionObject;

static void SubscriptionType_dealloc(SubscriptionObject *self)
{
//    printf("%s(%p)\n", __func__, self);
    if (PDCOM(self))
        Process_unsubscribe(PDCOM(self)->process, PDCOM(self));
    Py_XDECREF(self->variable);
    Py_XDECREF(self->subscriber);
    Py_XDECREF(self->process);

    Py_TYPE(self)->tp_free(self);
}

////////////////////////////////////////////////////////////////////////////
// Funcitons to convert from c-types to PyNumbers
// They rely on definitions in type_traits.h
////////////////////////////////////////////////////////////////////////////
template <typename T>
    static PyObject *
Get_Value(PyObject *(*to_pyobject)(T val),
        const PdCom::Variable::Subscription* s, size_t idx, size_t nelem)
{
    T val[nelem];
    s->getValue(val, idx, nelem);

    PyObject *obj;
    if (nelem == 1) {
        // Scalar object
        obj = to_pyobject(*val);
    } else {
        // Create tuple of objects
        obj = PyTuple_New(nelem);
        for (size_t i = 0; i < nelem; ++i)
            PyTuple_SET_ITEM(obj, i, to_pyobject(val[i]));
    }

    return obj;
}

template <typename T>
    static PyObject *
GetValue(const PdCom::Variable::Subscription* s, size_t idx, size_t nelem)
{
    return Get_Value(traits<T>::PyObject_From, s, idx, nelem);
}

////////////////////////////////////////////////////////////////////////////
// PyObject methods
////////////////////////////////////////////////////////////////////////////
static PyObject* subscription_getValue(SubscriptionObject *o, PyObject* args)
{
    unsigned int idx = 0;
    unsigned int nelem = PDCOM(o)->variable->nelem;
    if (!PyArg_ParseTuple(args, "|II", &idx, &nelem))
        return NULL;

    if (!nelem)
        Py_RETURN_NONE;

    if (idx + nelem > PDCOM(o)->variable->nelem) {
        PyErr_SetString(PyExc_IndexError, "index out of range");
        return NULL;
    }

    return o->getValue(PDCOM(o), idx, nelem);
}

////////////////////////////////////////////////////////////////////////////
static PyObject* subscription_poll(SubscriptionObject *o)
{
    PDCOM(o)->poll();
    Py_RETURN_NONE;
}

////////////////////////////////////////////////////////////////////////////
static PyObject* subscription_time(SubscriptionObject *o)
{
    return PyLong_FromUnsignedLongLong(**PDCOM(o)->time_ns);
}

////////////////////////////////////////////////////////////////////////////
static PyMethodDef tp_methods[] = {
    {"getValue", (PyCFunction)subscription_getValue, METH_VARARGS,
        PyDoc_STR("getValue(idx,nelem) -> values")},
    {"poll", (PyCFunction)subscription_poll, METH_NOARGS,
        PyDoc_STR("poll() -> poll for values")},
    {"time", (PyCFunction)subscription_time, METH_NOARGS,
        PyDoc_STR("time() -> time of values")},
    {0,0}
};

////////////////////////////////////////////////////////////////////////////
static PyMemberDef tp_members[] = {
    {(char*)"id", T_INT, offsetof(SubscriptionObject, id),
        READONLY, (char*)"id from request"},
    {(char*)"variable", T_OBJECT, offsetof(SubscriptionObject, variable),
        READONLY, (char*)"variable object"},
    {(char*)"decimation", T_INT, offsetof(SubscriptionObject, decimation),
        READONLY, (char*)"subscription decimation"},
//    {(char*)"process", T_OBJECT, offsetof(VariableObject, process),
//        READONLY, (char*)"process object"},
    {NULL}
};


static PyType_Slot tp_slots[] = {
    {Py_tp_doc, (void*)"The Subscription type"},
    {Py_tp_methods, tp_methods},
    {Py_tp_members, tp_members},
    {Py_tp_dealloc, (void*)SubscriptionType_dealloc},
    {0, 0}
};

static PyType_Spec type_spec = {
    "pdcom.Variable.Subscription",
    sizeof(SubscriptionObject),     /* basicsize */
    0,                          /* itemsize */
    Py_TPFLAGS_DEFAULT,
    tp_slots
};

PyObject * SubscriptionType_Object()
{
    static PyObject* obj;
    if (!obj)
        obj = PyType_FromSpec(&type_spec);
    return obj;
}

PyObject *Subscription_New(PyObject *subscriber, PyObject* process,
        const PdCom::Variable::Subscription *subscription)
{
    SubscriptionObject* self = PyObject_New(
            SubscriptionObject, (PyTypeObject*)SubscriptionType_Object());

    Py_INCREF(process);
    Py_INCREF(subscriber);

    self->process = process;
    self->subscriber = subscriber;
    self->pdcom = subscription;

    self->id = subscription->id;
    self->decimation = subscription->decimation;
    self->variable = Process_getVariableObject(
            subscription->process, subscription->variable);

    switch (self->pdcom->variable->type) {
        case PdCom::Variable::boolean_T:
            self->getValue = GetValue<    bool>;
            break;

        case PdCom::Variable::uint8_T:
            self->getValue = GetValue< uint8_t>;
            break;

        case PdCom::Variable:: int8_T:
            self->getValue = GetValue<  int8_t>;
            break;

        case PdCom::Variable::uint16_T:
            self->getValue = GetValue<uint16_t>;
            break;

        case PdCom::Variable:: int16_T:
            self->getValue = GetValue< int16_t>;
            break;

        case PdCom::Variable::uint32_T:
            self->getValue = GetValue<uint32_t>;
            break;

        case PdCom::Variable:: int32_T:
            self->getValue = GetValue< int32_t>;
            break;

        case PdCom::Variable::uint64_T:
            self->getValue = GetValue<uint64_t>;
            break;

        case PdCom::Variable:: int64_T:
            self->getValue = GetValue< int64_t>;
            break;

        case PdCom::Variable::double_T:
        case PdCom::Variable::single_T:
            self->getValue = GetValue<  double>;
            break;
    }

    return OBJ(self);
}
