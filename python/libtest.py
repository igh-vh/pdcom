#!/usr/bin/env python3

######################################################
# Python PdCom example
######################################################

# get the modules
import pdcom
import time, os, sys
from socket import socket, AF_INET, SOCK_STREAM
from sys import getrefcount
from random import randrange

class Subscriber(pdcom.Subscriber):
    _active = False

    def __init__(self):
        pdcom.Subscriber.__init__(self)

        Subscriber._active = True

        self.__subscription = {}
        self.__invalids = 0

        self.singles = []
        self.groups = []

    def __del__(self):
        Subscriber._active = False

    def newGroupValue(self, time):
        self.groups.append(len(self.__subscription))
        for i in self.__subscription.keys():
            self.__subscription[i].append(i.getValue())

    def newValue(self, s):
        self.singles.append(s)
        self.__subscription[s].append(s.getValue())

    def active(self, path, s):
        print(f"{path} is active {s.variable}")
        self.__subscription[s] = []

    def invalid(self, process, path, n):
        print(path, "is invalid")
        self.__invalids += 1

    def clear(self):
        del self.__subscription

    def replies(self):
        return len(self.__subscription) + self.__invalids

    def print(self):
        print(self.singles)
        print(self.groups)
        for i in self.__subscription.keys():
            print(f"{i.variable} = {self.__subscription[i]}")

class Process(pdcom.Process):
    active = False

    def __init__(self, addr):
        # Initialize instances
        pdcom.Process.__init__(self)

        Process.active = True
        self.__addr = addr

        # Create sockets and connect to server
        self.__socket = socket(AF_INET, SOCK_STREAM)
        self.__socket.connect(addr)

        # Duplicate the write channel to use buffered IO
        self.__fd = os.fdopen(self.__socket.fileno(), "wb")

        self.__connected = False
        self.asyncData(lambda: not self.__connected)

    def __del__(self):
        Process.active = False

    def applicationName(self):
        return "python pdcom"

    def hostname(self):
        return self.__addr[0]

    def asyncData(self, pred):
        while pred():
            if (pdcom.Process.asyncData(self) <= 0):
                raise "Process failed"

    def read(self, n):
        return self.__socket.recv(n)

    def write(self, buf):
        self.__fd.write(buf)

    def flush(self):
        self.__fd.flush()
        return 0    # Flush is always successful

    def connected(self):
        self.__connected = True

    def list(self, *path):
        self.__variables = []
        self.__directories = []

        self.__replies = 1
        pdcom.Process.list(self, *path)
        self.asyncData(lambda: self.__replies);

        v = self.__variables
        del self.__variables

        return (v,self.__directories)

    def listReply(self, variables, directories):
        self.__replies -= 1

        self.__variables.extend(variables)
        self.__directories.extend(directories)

    def find(self, path):
        self.__variable = 1

        pdcom.Process.find(self, path)
        self.asyncData(lambda: isinstance(self.__variable, int))

        v = self.__variable
        del self.__variable
        return v

    def findReply(self, var):
        self.__variable = var

    def ping(self):
        self.__pingstart = time.perf_counter()
        self.__pingend = 0.0;

        pdcom.Process.ping(self)
        self.asyncData(lambda: not self.__pingend)
        return self.__pingend - self.__pingstart;

    def pingReply(self):
        self.__pingend = time.perf_counter()

    def activeMessages(self):
        self.__messages = None

        pdcom.Process.activeMessages(self)
        self.asyncData(lambda: self.__messages is None)
        return self.__messages

    def activeMessagesReply(self, args):
        self.__messages = args

    def processMessage(self, *args):
        print(*args)

    def getMessage(self, m_id):
        self.__message = None
        pdcom.Process.getMessage(self, m_id)
        self.asyncData(lambda: self.__message is None)
        return self.__message

    def getMessageReply(self, *args):
        self.__message = args


print(getrefcount('pdcom.Variable'))

def refcount():
    return [getrefcount(n) for n in (Process, Process.read)]

refs = refcount()

def test_process():
    print("Test creation and destruction of process object")
    p = Process(("lappi",2345))
    print("name = {}, version = {}".format(p.name(), p.version()))
    assert Process.active, "Process must be active"
    del p
    assert not Process.active, "Process must be inactive"
    assert refs == refcount(), "refcount"
#
#print("Test deletion of subscriber before process")
#s = Subscriber()
#p = Process(("lappi",2345))
#p.subscribe(s, "some var", 0.1, 0)
#del s
#assert not Subscriber._active, "Subscriber must be inactive"
#del p
#
#print("Test deletion of process before subscriber")
#s = Subscriber()
#p = Process(("lappi",2345))
#p.subscribe(s, "some var", 0.1, 0)
##assert Process.active, "Process must be active, held by subscriber"
#del p
#del s
#assert not Subscriber._active, "Subscriber must be inactive"
#
#s = Subscriber()
#p = Process(("lappi",2345))
#p.subscribe(s, "some var", 0.1, 0)
#assert Process.active, "Process must be active, held by subscriber"
#del s
#assert not Subscriber._active, "Subscriber must be inactive"
#p.asyncData()
#print("end")
#
#s = Subscriber()
#p = Process(("lappi",2345))
#p.subscribe(s, "some var", 0.1, 0)
#p.asyncData()
#print("end1")
#del s
#del p

def test_list():
    print("Test list")
    p = Process(("lappi",2345))
    v,d = p.list()
    sig = [i.path for i in v if i.sampleTime]
    param = [i.path for i in v if not i.sampleTime]
    if v:
        v = v[randrange(0, len(v))]
        print("variable", v)
        print("    process", v.process)
        print("    type", v.type)
        print("    ctype", v.ctype)
        print("    sampletime", v.sampleTime)
        print("    writeable", v.writeable)
        print("    alias", v.alias)
        print("    path", v.path)
        print("    name", v.name)
        print("    nelem", v.nelem)
        print("    dim", v.dim)
        print("    task", v.task)
        print("    isScalar", v.isScalar)
        print("    isVector", v.isVector)

    p.list(None)
    p.list("")
    print(p.list("/"))

    return sig, param, d

s,p,d = test_list()

assert not Process.active, "Process must be inactive"

def test_variable(pathlist):
    path = pathlist[randrange(0,len(pathlist))]

    print("Test variable find")
    p = Process(("lappi",2345))
    refs = getrefcount('pdcom.Variable')
    v  = p.find(path)
    assert v is not None, "Variable should have been be found"
    v is not None, "Variable should have been be found"
    cnt = getrefcount(v)
    print(f"Found variable {v} refcount {cnt}")
    v1 = p.find(path)
    assert v is v1, "Variables must be the same"
    assert p.find('/') is None, "Find unkown path failed"
    assert getrefcount(v1) - cnt == 1, "Refcount failure 1"
    assert getrefcount(v1) - getrefcount(v) == 0, "Refcount failure 2"
    del v1
    assert getrefcount(v) - cnt == 0, "Refcount failure 3"
    del p
    assert Process.active, "Process must be active, held by variable"
    del v
    assert refs == getrefcount('pdcom.Variable'), "Variable refcount failed 2"
    assert not Process.active, "Process must be inactive"
test_variable(p)

def test_ping():
    print("Test ping")
    p = Process(("lappi",2345))
    print(p.ping())
    del p
    assert not Process.active, "Process must be inactive"
test_ping()

def test_messages():
    print("Test messages")
    p = Process(("lappi",2345))
    m = p.activeMessages()
    m_id = m[randrange(0, len(m))][0]
    print(p.getMessage(m_id))
    del p
    assert not Process.active, "Process must be inactive"
test_messages()

def test_subscriber(sigs, params):
    s = Subscriber()
    p = Process(("lappi",2345))

    for i in range(20):
        v = sigs[randrange(0,len(sigs))]
        print(f"Subscribing to {v}")
        p.subscribe(s, v, 0.1, i)

    for i in range(5):
        p.subscribe(s, f"/abc{i}", 0.1, i)

    for i in range(5):
        v = params[randrange(0,len(params))]
        print(f"Subscribing to {v}")
        p.subscribe(s, v, 0.1, i)

    p.asyncData(lambda: s.replies() != 30)

    p.asyncData(lambda: len(s.groups) < 20)

    s.print()
    s.clear()
test_subscriber(s,p)
