This is the README file of the PdCom library, that is part of the EtherLab
project (http://etherlab.org/en). The home of PdCom is
http://etherlab.org/en/pdcom. The Library is released under the terms and
conditions of the GNU Lesser General Public License (LGPL), version 3 or (at
your option) any later version.

## Documentation

The library documentation resides in the header files and can be brought to
HTML, LaTeX and man format with the doxygen tool. To generate the
documentation, make sure that doxygen is installed and then call:

```sh
mkdir build
cd build
cmake ..
make doc
```

Then point your favourite browser to doc/html/index.html.

## Building and Installing

To just build and install the library, call:

```sh
mkdir build
cd build
cmake ..
make
make install
```

## Using PdCom4 in your own application

Modules for pkg-config (libpdcom4) and CMake are provided.
CMake example:

```cmake
cmake_minimum_required(VERSION 3.2)
project(myexample)
find_package(PdCom4 REQUIRED)
add_executable(myexample main.cpp)
target_link_libraries(myexample PUBLIC EtherLab::pdcom4)
```
If PdCom4 is installed into a non-standard folder,
`CMAKE_PREFIX_PATH` and/or `PKG_CONFIG_PATH` have to be adapted.

## Further Information

For questions of any kind, subscribe to the etherlab-users mailing list at
http://etherlab.org/en.

Have fun with PdCom!

