$Id$

[2021-04-21]
Version 4.2.0
- small fixes

- changes to be aware of:
  * python interface reworked; removed boost depenency
  * Subscriber::invalid() got a new overloaded virtual method; old method is
    deprecated
  * The mechanics of requesting message history is completely reworked;
    - Process::messageHistory() and messageHistoryEnd() removed
    - Process::processMessage() got a real knock on the head

[2018-06-07]
Version 4.1.0
- Header files moved: The header files used to be pdcom.h and pdcom/*.h.
  These have now been moved to pdcom4.h and pdcom4/*.h to enable parallel
  installation with stable-3.0

- new features:

  * broadcast(), broadcastReply(): used to transmit and receive messages
    between all clients

  * transmitSemaphore(): used in multithreaded applications to serialize
    transmitted commands on the network.

  * messageHistoryEnd(): used to signal the end of messageHistory()

- changes to be aware of:

  * messageHistory(): due to improvements in the protocol, the messages come in
    reverse order when calling messageHistory(). If you used messageHistory(),
    this is an incompatable change and you must rework processMessage()

  * processMessage(): A parameter "text" is new and bears a string with
    the message text.

- python interface
  * changed to reflect the C++ changes
  * version now a string instead of number list

----------------------------------------------
Version 4.0.0
