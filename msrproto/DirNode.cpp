/*****************************************************************************
 *
 * $Id$
 *
 * Copyright (C) 2015-2016  Richard Hacker (lerichi at gmx dot net)
 *
 * This file is part of the PdCom library.
 *
 * The PdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The PdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#include "DirNode.h"

#include "../Debug.h"

#include <algorithm>    // std::copy
#include <iterator>     // back_inserter
#include <cstring>      // strchr()

///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
class DirectoryPathIterator {
    public:
        DirectoryPathIterator(const char* path) {
            pathPtr = path;
        }

        std::string next() {
            const char* p = pathPtr;
            pathPtr = strchr(pathPtr, dirSeparator);
            return pathPtr ? std::string(p, skip() - p) : p;
        }

        bool hasNext() const {
            return pathPtr and *pathPtr;
        }

    private:
        const char* pathPtr;

        static const char dirSeparator;

        const char* skip() {
            const char* p = pathPtr;

            while (*pathPtr and *pathPtr == dirSeparator)
                ++pathPtr;

            return p;
        }
};

const char DirectoryPathIterator::dirSeparator = '/';

///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
DirNode::DirNode(bool isDir)
{
    m_parent = this;
    directory = isDir;
}

///////////////////////////////////////////////////////////////////////////
DirNode::~DirNode()
{
    for (NodeVector::iterator it = children.begin();
            it != children.end(); ++it)
        delete *it;
}

///////////////////////////////////////////////////////////////////////////
std::string DirNode::path() const
{
    return m_parent == this
        ? std::string()
        : (m_parent->path() + '/' + m_name);
}

///////////////////////////////////////////////////////////////////////////
void DirNode::insert(DirNode* child, const char* cpath)
{
    DirectoryPathIterator path(cpath);
    DirNode* parent = this;

    if (!path.hasNext()) {
        delete child;
        return;
    }

//    std::cout << __LINE__ << ' ' << cpath << std::endl;
    while (true) {
        std::string name = path.next();

        if (name.empty()) {
            // Find root node
            parent = getRootNode();
            continue;
        }

        NodeVector::iterator it =
            std::lower_bound(parent->children.begin(), parent->children.end(),
                    name, LessThan());

        // Insert a new node if it exists
        if (it == parent->children.end() or (*it)->m_name != name)
            it = parent->children.insert(it, 0);
        DirNode*& node = *it;

        if (path.hasNext()) {
            if (!node) {
                node = new DirNode;
                node->m_parent = parent;
                node->m_name = name;
//                std::cout << "New node " << node->path() << std::endl;
            }
            parent = node;
        } else {
            if (node) {
                // Node exists. This can happen, when a deep node has
                // been discovered before the current node. An example is a
                // signal that has parameters as children
                std::swap(node->children, child->children);
                child->directory = true;
                for (NodeVector::iterator it = child->children.begin();
                        it != child->children.end(); ++it)
                    (*it)->m_parent = child;

                delete node;

//                std::cout << "Replaced node " << child->path() << std::endl;
            }

            node = child;
            child->m_parent = parent;
            child->m_name = name;
//            std::cout << "New leaf " << child->path() << std::endl;

            return;
        }
    }
}

///////////////////////////////////////////////////////////////////////////
std::string DirNode::name() const
{
    return m_name;
}

///////////////////////////////////////////////////////////////////////////
DirNode* DirNode::find(const std::string& pathStr)
{
    DirNode *node = this;
    DirectoryPathIterator path(pathStr.c_str());

    while (path.hasNext()) {
        std::string name = path.next();

        if (name.empty()) {
            // Find root node
            node = getRootNode();
            continue;
        }
//        else if (name == ".") {
//            continue;
//        }
//        else if (name == "..") {
//            node = node->m_parent;
//            continue;
//        }

        NodeVector::iterator it =
            std::lower_bound(node->children.begin(), node->children.end(),
                    name, LessThan());

        if (it == node->children.end() or name != (*it)->m_name)
            return 0;

        node = *it;
    }

    return node;
}

///////////////////////////////////////////////////////////////////////////
void DirNode::getChildren(List* list) const
{
    std::copy(children.begin(), children.end(),
            std::back_inserter(*list));
}

///////////////////////////////////////////////////////////////////////////
void DirNode::getAllChildren(List* list) const
{
    for (NodeVector::const_iterator it = children.begin();
            it != children.end(); ++it) {
        list->push_back(*it);
        (*it)->getAllChildren(list);
    }
}

///////////////////////////////////////////////////////////////////////////
bool DirNode::hasChildren() const
{
    return directory or !children.empty();
}

/////////////////////////////////////////////////////////////////////////////
DirNode* DirNode::getRootNode()
{
    DirNode *n = this;
    while (n->m_parent != n)
        n = n->m_parent;
    return n;
}

