/*****************************************************************************
 *
 * $Id$
 *
 * Copyright (C) 2015-2016  Richard Hacker (lerichi at gmx dot net)
 *
 * This file is part of the PdCom library.
 *
 * The PdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The PdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#ifndef PD_MSRPROTOCOLHANDLER_H
#define PD_MSRPROTOCOLHANDLER_H

#include "../ProtocolHandler.h"
#include "../StreambufLayer.h"
#include "../pdcom4/Process.h"
#include "MsrVariable.h"

#include <expat_external.h>
#include <expat.h>                                                              
#include <stddef.h>
#include <string>
#include <ostream>
#include <queue>
#include <vector>
#include <map>
#include <set>

class Channel;
class Parameter;
class DataGroup;
struct Request;

namespace msr {

struct XmlStream: std::ostream {
    XmlStream(PdCom::Process* process, std::streambuf* buf);
    void lock();
    void unlock();
    PdCom::Process* process;
};

class ProtocolHandler:
    public ::ProtocolHandler,
    public ::StreambufLayer
{
    public:
        ProtocolHandler(PdCom::Process* process, IOLayer* io);
        ~ProtocolHandler();

        static ::ProtocolHandler* create(PdCom::Process*,
                IOLayer* io, const std::string& data);

        MsrVariable::Subscription* subscribe(Channel*, Request* r);
        void unsubscribe(Channel*, int groupId);
        void unsubscribe(const PdCom::Variable::Subscription* s);

        void pollChannel(size_t index);
        void pollParameter(size_t index, bool verbose, bool parameterMonitor);
        void set(const Parameter* p, size_t offset,
                const std::string& s);

    private:
        size_t parse(const char *buf, size_t n);
        XML_Parser xmlParser;
        int level;
        bool polite;
        bool tls;
        bool protocolError;
        uint64_t dataTime;

        std::string m_name;
        std::string m_version;

        XmlStream cout;

        DirNode* const root;

        // Structures required for list command
        std::queue<std::string> listQ;
        bool processListRequest();
        enum {
            Uncached, InProgress, Cached
        } fullListing;

        std::queue<std::string> findQ;
        bool processFindRequest();

        // Structures required for login
        void processLogin(const char** atts);

        bool processLogMessage(const char* name,
                const char** atts, bool history, bool record);
        bool parseMessage(PdCom::Process::Message& m,
                const char* name, const char** atts);

        struct Feature {
            bool pushparameters, binparameters, eventchannels,
                 statistics, pmtime, aic, messages,
                 quiet, list, exclusive, polite, xsadgroups, tls;
            int login;
            unsigned eventid;
            void operator=(const char* list);
        };
        Feature feature;

        typedef std::queue<Request*> RequestQ;
        typedef std::map<std::string, RequestQ> PendingMap;
        PendingMap pendingMap;
        std::queue<PendingMap::iterator> pendingQ;
        std::queue<DataGroup*> xsadQ;

        std::queue<uint32_t> seqNoQ;
        uint32_t messageSeqNo;
        std::list<PdCom::Process::Message> messageList;

        typedef std::set<Request*> RequestSet;
        RequestSet requestSet;

        typedef std::map<PdCom::Subscriber*, RequestSet>
            SubscriberMap;
        SubscriberMap subscriberMap;

        typedef std::set<PdCom::Subscriber*> SubscriberSet;
        SubscriberSet parameterMonitorSet;

        typedef std::map<unsigned, Channel*> ChannelMap;
        ChannelMap channel;
        typedef std::map<unsigned, Parameter*> ParameterMap;
        ParameterMap parameter;

        std::queue<Channel*> eventQ;
        std::queue<Request*> requestQ;

        std::vector<DataGroup*> dataGroup;
        DataGroup* currentDataGroup;

        struct Task {
            std::map<unsigned, DataGroup*> dataGroup;
        };
        std::map<unsigned, Task> task;

        enum {StartUp, Idle, GetListing,
            WaitForConnected, GetVariableFields, ReadData, ReadEvent, 
            GetActiveMessages
        } state;

        void setPolite(bool state);

        /** Read a variable tag */
        bool getDataType(const char** atts,
                PdCom::Variable::Type& type, size_t& ndim, size_t *dim);
        Parameter* getParameter(const char** atts);
        Channel*   getChannel(  const char** atts);

        void startElement(const XML_Char *name, const XML_Char **atts);
        void endElement(const XML_Char *name);
        void characterData(const XML_Char *name, int len);

        static void XMLCALL ExpatStartElement(void *userData,
                const XML_Char *name, const XML_Char **atts);
        static void XMLCALL ExpatEndElement(void *userData,
                const XML_Char *name);
        static void XMLCALL ExpatCharacterData(void *userData,
                const XML_Char *name, int len);

        void xsadAck();
        void eventAck();
        void pendingAck(MsrVariable*);
        void messageHistoryAck();

        // Reimplemented from PdCom::ProtocolHandler
        int connect();
        int asyncData();
        bool find(const std::string& path);
        bool list(const std::string& directory);
        void logout();
        bool login(const char* mech, const char* clientData);
        void   subscribe(PdCom::Subscriber*,
                const std::string& path, double interval, int id);
        void unsubscribe(PdCom::Subscriber*);
        void parameterMonitor(PdCom::Subscriber*, bool state);
        void getMessage(uint32_t seqNo);
        void getActiveMessages();
        void broadcast(const std::string&, const std::string&);
        bool startTLS();
        void ping();
        std::string name() const;
        std::string version() const;
};

} // namespace

#endif // PD_MSRPROTOCOLHANDLER_H
